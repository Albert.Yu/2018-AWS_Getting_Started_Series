# 2018-AWS_Getting_Started_Series

## 第一階段7/25與8/1內容
[第一階段影片](https://www.youtube.com/embed/videoseries?list=PLOM231PThjF_0bYoPIJGfjeiey6XIg76D)

    活動結束後大家對於AWS Elastic Beanstalk 有相當的了解了
    如果對於建置還有些不了解的
    可以參考上列連結影片來進行設定


## 第二階段8/28內容
    在上個階段大家已經了解了AWS Elastic Beanstalk的建置
    但是在網站的建置已經不如十年前只靠硬體來撐附載及攻擊
    本次內容主要會是在
    1. AWS Cloudfront
    2. AWS WAF
    3. AWS Route53